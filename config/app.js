const convict = require('convict')

const config = convict({
  app: {
    name: {
      doc: 'Test Application',
      format: String,
      default: 'Test Application'
    }
  },
  env: {
    doc: 'The application environment.',
    format: ['production', 'development', 'staging'],
    default: 'development',
    env: 'NODE_ENV'
  },
  port: {
    doc: 'The port to bind.',
    format: 'port',
    default: 12345,
    env: 'port'
  },
  namespace: {
    doc: '',
    format: String,
    default: 'v1',
    env: 'NAMESPACE'
  },
  logs: {
    directory: {
      doc: 'Directory where logs found',
      format: String,
      default: 'logs',
      env: 'LOG_DIRECTORY'
    },
    level: {
      doc: 'level of log',
      format: String,
      default: 'debug',
      env: 'LOG_LEVEL'
    },
  },
  twilio: {
    format: Object,
    default: {
      accountSid: 'ACxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
      authToken: '46xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
      callbackURL: 'http://dummy-12345.ngrok.io',
      applicationSid: 'APxxxxxxxxxxxxxxxxxxxxxxxxxx',
    },
  },
  devices: {
    default: {
      device1: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', // Put Device name here
    },
  }
})

const env = config.get('env')
const envFilePath = `./environment/${env}.json`

config.loadFile(envFilePath)

config.validate({ allowed: 'strict' })

module.exports = config
