# twilio-calling-server
Simple Node Express Calling Server
# Basic Calling Functionality Configuration

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

This is a backend code for Calling and its frontend can be found [ here ] ( https://developmentmadeeasy2020@bitbucket.org/developmentmadeeasy2020/twilio-calling-client.git )

### Prerequisites

   1) Install ngrok and at its directory, tunnel the localhost at 12345, using the ./ngrok http 12345 ( For Unix ) or ngrok.exe http 12345 ( For windows ) copy the url which looks something like http://12345dummy.ngrok.io.

# Configuration
 1) git checkout feature/calling

 2) Follow all the listed instructions at feature/calling
